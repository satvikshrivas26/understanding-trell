package model

type Medals struct {
	Id      int64  `db:"Id" json:"id"`
	Country string `db:"country" json:"country"`
	Gmedal  int64  `db:"gMedal" json:"gMedal"`
	Smedal  int64  `db:"sMedal" json:"sMedal"`
	Bmedal  int64  `db:"bMedal" json:"bMedal"`
}
