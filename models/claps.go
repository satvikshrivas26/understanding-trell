package model

type Claps struct {
	Id      int64  `db:"Id" json:"-"`
	Country string `db:"Country" json:"country"`
	Claps   int64  `db:"Claps" json:"claps"`
}
