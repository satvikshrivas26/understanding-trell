package model

type Scheduler struct {
	Id        int64  `db:"Id" json:"id"`
	Eventname string `db:"eventName" json:"country"`
	EventDate string `db:"eventDate" json:"date"`
}
