FROM golang:1.17.1-alpine3.14

WORKDIR /self

COPY go.mod .

COPY go.sum .

RUN go mod download

COPY . .

ENV PORT 4200

RUN go build

EXPOSE $PORT

CMD ["./understanding-trell"]