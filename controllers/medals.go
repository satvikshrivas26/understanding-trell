package controllers

import (
	"fmt"
	"strconv"

	model "gitlab.com/shivam.jha/understanding-trell/models"

	"github.com/gin-gonic/gin"
	_ "github.com/go-sql-driver/mysql"
)

func Getmedal(c *gin.Context) {
	var user []model.Medals
	_, err := dbmap.Select(&user, "select * from medal")
	if err == nil {
		c.JSON(201, user)
	} else {
		c.JSON(404, gin.H{"error": "user not found"})
	}
}

func UpdateMedal(c *gin.Context) {
	var user model.Medals
	country := c.PostForm("country")
	Smedal, _ := strconv.ParseInt((c.PostForm("sMedal")), 0, 64)
	Bmedal, _ := strconv.ParseInt((c.PostForm("bMedal")), 0, 64)
	Gmedal, _ := strconv.ParseInt((c.PostForm("gMedal")), 0, 64)
	err := dbmap.SelectOne(&user, "select * from medal WHERE country=?", country)
	if err == nil {
		user.Gmedal += Gmedal
		user.Smedal += Smedal
		user.Bmedal += Bmedal
		_, error := dbmap.Exec("UPDATE medal SET medal.gMedal=? ,medal.sMedal=?, medal.bMedal=? WHERE medal.Country=? ", user.Gmedal, user.Smedal, user.Bmedal, user.Country)
		fmt.Println(user, error, Gmedal, Smedal, Bmedal)

		c.JSON(200, gin.H{"success": "Updated"})

	} else {
		dbmap.Exec("INSERT INTO  medal (Id,country,gMedal,sMedal,bMedal) VALUES(?,?,?,?,?) ", user.Id, country, Gmedal, Smedal, Bmedal)
		c.JSON(404, gin.H{"success": "Added"})
	}
}
